# Don't prompt user for sudo password
#echo "$USER ALL=(ALL:ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/dont-prompt-$USER-for-sudo-password
echo "$USER ALL=(ALL:ALL) NOPASSWD: /usr/bin/pacman" | sudo tee /etc/sudoers.d/dont-prompt-$USER-for-sudo-password-pacman

# Install system packages
sudo pacman -Syu --needed --noconfirm - < ./pkglist/arch_official.txt
yay         -Syu --needed --noconfirm - < ./pkglist/arch_user.txt

# Install configs
(cd stow && ./install.sh)

# Install neovim packages
nvim -c "execute('PlugUpgrade') | execute('PlugInstall') | execute('qa!')"

chsh -s /bin/zsh

echo "+------------------+"
echo "| Install complete |"
echo "+------------------+"
