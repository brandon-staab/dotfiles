conky.config = {
	-- General
	cpu_avg_samples    = 10,
	diskio_avg_samples = 10,
	net_avg_samples    =  2,
	update_interval    =  2,
	imlib_cache_size   =  0,
	double_buffer      = true, -- Eliminates flicker

	-- Position
	alignment      = 'top_right', -- top_left, top_middle, top_right, bottom_left, bottom_middle, bottom_right
	gap_x          = 50,
	gap_y          = 74,
    minimum_height = 200,
	minimum_width  = 260,
	maximum_width  = 260,

	-- Textual
	font = 'Ubuntu:bold:size=10',
	top_name_width = 21,
	max_text_width = 0,           -- 0 will make sure line does not get broken if width too smal
	use_xft = true,               -- Anti-aliased font

	-- Window
	own_window = true,
	own_window_type = 'override', -- Override window manager's control
	own_window_class = 'Conky',        -- manually set the WM_CLASS name for use with xprop
	own_window_transparent = true,     -- if own_window_argb_visual is true sets background opacity 0%

	-- Colors (Solarized Dark)
	color0 = '#2aa198',
	color1 = '#fdf6e3',
	color2 = '#268bd2',
	color3 = '#d33682',
};

conky.text = [[
	${voffset 2}${alignc}${color0}${utime %H:%M}${font}${color}
	${alignc}${color0}${font Ubuntu:size=30}${time %H:%M}
	${voffset 6}${alignc}${font Ubuntu:size=14}${time %y/%m/%d}
	${voffset 6}${alignc}${time %A in %B}
	${voffset 18}${goto 12}${color3}CPU${goto 50}$cpu%
	${color2}${goto 12}${cpubar 8,254}
	${voffset 5}${goto 12}${color1}${top name 1}$alignr$color${top cpu 1}%
	${goto 12}${color1}${top name 2}$alignr$color${top cpu 2}%
	${goto 12}${color1}${top name 3}$alignr$color${top cpu 3}%
	${goto 12}${color1}${top name 4}$alignr$color${top cpu 4}%
	${goto 12}${color1}${top name 5}$alignr$color${top cpu 5}%
	${voffset 14}${goto 12}${color3}RAM${goto 50} ${mem}/${memmax}
	${color2}${goto 12}${membar 8,254}${color}
	${goto 12}${voffset 5}${color1}${top_mem name 1}$alignr$color${top_mem mem_res 1}
	${goto 12}${color1}${top_mem name 2}$alignr$color${top_mem mem_res 2}
	${goto 12}${color1}${top_mem name 3}$alignr$color${top_mem mem_res 3}
	${goto 12}${color1}${top_mem name 4}$alignr$color${top_mem mem_res 4}
	${goto 12}${color1}${top_mem name 5}$alignr$color${top_mem mem_res 5}
]];
