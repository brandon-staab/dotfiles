"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Sections:
"    -> Plugins
"    -> General
"    -> VIM user interface
"    -> Colors and Fonts
"    -> Files and backups
"    -> Text, tab and indent related
"    -> Visual mode related
"    -> Moving around, tabs and buffers
"    -> Status line
"    -> Editing mappings
"    -> Searching
"    -> Spell checking
"    -> Misc
"    -> Helper functions
"    -> Hooks
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:rplugin_home = expand("$HOME/.cache/nvim/plugged/")

" Plugin Manager
if ! filereadable(expand("$HOME/.config/nvim/autoload/plug.vim"))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p "$HOME/.config/nvim/autoload/"
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > "$HOME/.config/nvim/autoload/plug.vim"
	autocmd VimEnter * PlugInstall
endif

" Plugin List
call plug#begin("$HOME/.cache/nvim/plugged")
	let g:plug_timeout = 6000

	Plug 'tomasiser/vim-code-dark'
	Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
	" main interface
	Plug 'bling/vim-airline'
	Plug 'preservim/nerdtree'
	Plug 'christoomey/vim-tmux-navigator'
	Plug 'ryanoasis/vim-devicons'

	" parentheses
	Plug 'tmsvg/pear-tree' " pairs parentheses, quotes, ...
	Plug 'luochen1990/rainbow' " colors parentheses, ...

	" git
	Plug 'APZelos/blamer.nvim'
	Plug 'airblade/vim-gitgutter'
	Plug 'jreybert/vimagit'
	Plug 'Xuyuanp/nerdtree-git-plugin'

	" tooling
	Plug 'vim-syntastic/syntastic'
	Plug 'ycm-core/YouCompleteMe' , {'do': './install.py --all --clangd-completer'}
	Plug 'rhysd/vim-clang-format'

	" file support
	Plug 'jalvesaq/Nvim-R', {'branch': 'stable', 'for': 'R'}
	Plug 'xuhdev/vim-latex-live-preview', {'for': 'tex'}

	" extras
	Plug 'tpope/vim-dadbod' " Databases
	Plug 'tpope/vim-surround'
call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set nocompatible							" Use modern terminal functionality
set encoding=utf-8							" Text encoding
set visualbell								" Use visual bell (no beeping)
set splitbelow splitright					" Default term location
set notimeout ttimeout ttimeoutlen=7		" Relax timeout
set confirm									" Always confirm commands
set clipboard+=unnamedplus					" Enable clipboard
set backspace=indent,eol,start				" Backspace mode

filetype plugin on
filetype indent plugin off

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set hidden						" Hidden buffers
set mouse=a						" Enable mouse

set showcmd
set number						" Show line numbers
"set relativenumber				" Show relative line numbers
set showmatch					" Highlight matching brace
set wildmenu					" Enable autocompletion
set wildmode=longest,list,full	" Autocompletion style

" TMUX
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <c-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <c-j> :TmuxNavigateDown<cr>
nnoremap <silent> <c-k> :TmuxNavigateUp<cr>
nnoremap <silent> <c-l> :TmuxNavigateRight<cr>
nnoremap <silent> <c-\> :TmuxNavigatePrevious<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
colorscheme codedark
set cursorline
let &colorcolumn="80,120,160,200"

highlight Comment      cterm=italic gui=italic
highlight CursorLineNr cterm=bold   gui=bold
highlight Visual       ctermbg=240  guibg=#585858

highlight GitGutterAdd     ctermfg=white ctermbg=darkgreen  guibg=darkgreen
highlight GitGutterChange  ctermfg=white ctermbg=darkyellow guibg=darkyellow
highlight GitGutterDelete  ctermfg=white ctermbg=darkred    guibg=darkred

syntax enable

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files and backups
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set tabstop=4
set shiftwidth=4				" Number of auto-indent spaces
set softtabstop=4				" Number of spaces per Tab
set autoindent					" Auto-indent new lines
set smartindent					" Enable smart-indent
set smarttab					" Enable smart-tabs

" Show whitespace
set list
set listchars=tab:→\ ,nbsp:␣,trail:•,extends:⟩,precedes:⟨,eol:↲
set linebreak					" Break lines at word (requires Wrap lines)
set showbreak=↪

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Visual mode related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Status line
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set cmdheight=2
set laststatus=2
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Editing mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader =","
nnoremap c "_c

map <leader>e :call ToggleWhitespace()<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Searching
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Find and replace
set hlsearch					" Highlight all search results
set smartcase					" Enable smart-case search
set ignorecase					" Always case-insensitive
set incsearch					" Searches for strings incrementally

" Clear search highlight on double escape without clearing register
nnoremap <esc><esc> :noh<return>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Spell checking
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <leader>s :set spell!<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Helper functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

function! ToggleWhitespace()
if &listchars == "tab:→\ ,nbsp:␣,trail:•,extends:⟩,precedes:⟨,eol:↲"
| set listchars=tab:→\ ,space:·,nbsp:␣,trail:•,extends:⟩,precedes:⟨,eol:↲
else
| set listchars=tab:→\ ,nbsp:␣,trail:•,extends:⟩,precedes:⟨,eol:↲
endif
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Hooks
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Change Color when entering Insert Mode
autocmd InsertEnter * highlight  CursorLine ctermbg=None ctermfg=None cterm=None guibg=none guifg=None

" Revert Color to default when leaving Insert Mode
autocmd InsertLeave * highlight  CursorLine ctermbg=238 ctermfg=None cterm=None guibg=#444444 guifg=None

autocmd BufNewFile,BufRead *.tmux set filetype=tmux

" Automatically deletes all trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//ge

" Delete empty or whitespaces-only lines at the end of file
autocmd BufWritePre * :%s/\(\s*\n\)\+\%$//ge

" Disables automatic commenting on newline
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Clang-Format
autocmd FileType c,cc,cpp,cxx,h,hpp,tpp,inl ClangFormatAutoEnable

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Turn persistent undo on
"    means that you can undo even when you close a buffer/VIM
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
try
	set undolevels=1000
	set undodir=$HOME/.cache/nvim/undodir
	set undofile
catch
endtry

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General abbreviations
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
iab xdate <C-r>=strftime(trim($TIME_STYLE, '+'))<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General plugin config
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Nerd tree
let g:NERDTreeWinPos = "left"
let NERDTreeShowHidden = 1
let g:NERDTreeGitStatusUseNerdFonts = 1
let NERDTreeIgnore = ['\.pyc$',  '__pycache__', '\.png', '\.o']

map <leader>nn :NERDTreeToggle<CR>
map <leader>nb :NERDTreeFromBookmark<Space>
map <leader>nf :NERDTreeFind<cr>

" Start NERDTree. If a file is specified, move the cursor to its window.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * NERDTree | if argc() > 0 || exists("s:std_in") | wincmd p | endif

" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

" Close the tab if NERDTree is the only window remaining in it.
autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

" If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
autocmd BufEnter * if bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
    \ let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif

" LaTeX
let g:livepreview_previewer = $READER

" Rainbow paren
let g:rainbow_active = 1

" YouCompleteMe
let g:ycm_key_list_stop_completion = ['<CR>']
let g:ycm_autoclose_preview_window_after_insertion = 1

map <leader>ct :YcmCompleter GetType<cr>
map <leader>cd :YcmCompleter GetDoc<cr>
map <leader>cf :YcmCompleter FixIt<cr>

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" GitGutter
set signcolumn=yes
autocmd BufWritePost * GitGutter

" Blamer
let g:blamer_enabled = 1
let g:blamer_show_in_visual_modes = 0
let g:blamer_show_in_insert_modes = 0
let g:blamer_date_format = trim($TIME_STYLE, '+')

"""""""""""""""""""""""""""
" Project specific config "
"""""""""""""""""""""""""""
set exrc
set secure
