export HISTORY_IGNORE="(..|.. *|cd|cd *|e|e *|exit|g lg|g pop|g pull|g push|g stash|g status|git lg|git pop|git pull|git push|git stash|git status|h|history|l.|l. *|ll|ls *|ls|ls *|mv|mv *|p -Syu*|ping*|pwd|rm|rm *sdn|sdn -r|shred *|tmux at|tree|up|up *|v|v *)"

export HISTFILE="$HOME/.cache/zsh/history"
mkdir -p "${HISTFILE%/*}"

export HISTSIZE=10000
export SAVEHIST=$HISTSIZE
setopt hist_ignore_all_dups
setopt hist_ignore_space
