# Set shell profile variables
[ -f "$HOME/.config/shell/profile" ] && source "$HOME/.config/shell/profile"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Start tmux on startup
if [[ -f "$HOME/.config/tmux/autostart.sh" ]]; then
	source "$HOME/.config/tmux/autostart.sh"
fi

typeset -g -A key
export KEYTIMEOUT=1

# Disable ctrl-s to freeze terminal
stty stop undef

# Source extras
source "$HOME/.config/zsh/history.zsh"
source "$HOME/.config/zsh/keybinds.zsh"
source "$HOME/.config/zsh/cursor.zsh"
source "$HOME/.config/zsh/theme.zsh"

setopt prompt_subst

# Tab complete
autoload -U compinit
zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'
compinit

# Include hidden files in autocomplete:
_comp_options+=(globdots)

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
	function zle_application_mode_stop { echoti rmkx }
	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi


# Patch stderr with yellow
if [[ -f "/usr/lib/libstderred.so" ]]; then
	export LD_PRELOAD="/usr/lib/libstderred.so${LD_PRELOAD:+:$LD_PRELOAD}"
	export STDERRED_ESC_CODE=$(echo -e '\033[33m')
fi

# Load zsh-syntax-highlighting; should be last.
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
